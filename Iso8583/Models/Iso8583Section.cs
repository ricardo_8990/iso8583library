﻿namespace Iso8583.Models
{
    public abstract class Iso8583Section
    {
        protected Iso8583Section(byte[] bytes)
        {
            IsValid();
        }

        internal abstract void IsValid();
    }
}