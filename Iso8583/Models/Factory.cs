﻿using System;

namespace Iso8583.Models
{
    public static class Factory
    {
        public static Iso8583Section Iso8583Section(Constants.IsoType type, Constants.IsoSections section, ref byte[] bytes)
        {
            switch (type)
            {
                case Constants.IsoType.V1987:
                    return V1987.Factory.Iso8583Section(section, ref bytes);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
