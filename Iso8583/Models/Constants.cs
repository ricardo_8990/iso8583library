﻿namespace Iso8583.Models
{
    public class Constants
    {
        public const int MaxByteLength = 126;
        public const int MinByteLength = 64;
        public enum IsoType
        {
            V1987
        }
        public enum IsoSections
        {
            Header,
            Mti,
            Bitmap,
            Data
        }
    }
}
