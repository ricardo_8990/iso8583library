﻿using System;
using Iso8583.Models;

namespace Iso8583.V1987
{
    public static class Factory
    {
        public static Iso8583Section Iso8583Section(Constants.IsoSections section, ref byte[] bytes)
        {
            switch (section)
            {
                case Constants.IsoSections.Header:
                    return new Header(ref bytes);
                case Constants.IsoSections.Mti:
                    return new Mti(ref bytes);
                case Constants.IsoSections.Bitmap:
                    return new Bitmap(ref bytes);
                case Constants.IsoSections.Data:
                    return new Data(ref bytes);
                default:
                    throw new ArgumentOutOfRangeException(nameof(section), section, null);
            }
        }
    }
}
