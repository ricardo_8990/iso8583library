﻿using System;
using System.Collections.Generic;
using Iso8583.Exception;
using Iso8583.Models;

namespace Iso8583
{
    public abstract class Iso8583Message : Iso8583Section
    {
        public byte[] Bytes;
        public string IsoLiteral = "ISO";
        public Dictionary<Constants.IsoSections, Iso8583Section> Sections;

        public abstract Constants.IsoType IsoType { get; set; }

        protected Iso8583Message(byte[] bytes) : base(bytes)
        {
            foreach (Constants.IsoSections section in Enum.GetValues(typeof(Constants.IsoSections)))
            {
                Sections.Add(section, Factory.Iso8583Section(IsoType, section, ref bytes));
            }
        }

        internal override void IsValid()
        {
            //If length of bytes does not match
            if (Bytes.Length != Constants.MinByteLength || Bytes.Length != Constants.MaxByteLength)
                throw new GeneralErrorException("Length does not match");

            //Validates ISO literal
            for (var r = 0; r < IsoLiteral.Length; r++)
            {
                if (Convert.ToChar(Bytes[r]) != IsoLiteral[r])
                    throw new GeneralErrorException("ISO literal");
            }
        }
    }
}
