﻿using System;
using System.Runtime.Serialization;

namespace Iso8583.Exception
{
    public class GeneralErrorException : System.Exception
    {
        public GeneralErrorException()
        {
            throw new NotImplementedException();
        }

        public GeneralErrorException(string message) : base(message)
        {
            throw new NotImplementedException();
        }

        public GeneralErrorException(string message, System.Exception innerException) : base(message, innerException)
        {
            throw new NotImplementedException();
        }

        protected GeneralErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            throw new NotImplementedException();
        }
    }
}
